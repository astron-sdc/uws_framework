try:
    from uws_procedures.tasks import TaskAdapter
except:
    TaskAdapter = object


class TestTaskAdapter(TaskAdapter):
    def __init__(self):
        print('Yeah it is me')

    def spawn_job(self, type, paramters):
        pass

    def get_job_phase(self, job_type, job_id):
        pass

    def get_job_errors(self, job_type, job_id):
        pass

    def delete_job(self, job_type, job_id):
        pass

    def get_tasks_list(self):
        pass

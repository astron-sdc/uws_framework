from django.apps import AppConfig


class UwsProceduresConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uws_procedures'

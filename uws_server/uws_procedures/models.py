from django.db import models
from django.utils.translation import gettext_lazy as _
from .tasks import TaskAdapter


class ExecutionPhase(models.TextChoices):
    # the job is accepted by the service but not yet committed for execution by the client.
    # In this state, the job quote can be read and evaluated.
    # This is the state into which a job enters when it is first created.
    PENDING = 'PENDING', _('PENDING')

    # the job is committed for execution by the client
    # but the service has not yet assigned it to a processor.
    # No Results are produced in this phase.
    QUEUED = 'QUEUED', _('QUEUED')

    # the job has been assigned to a processor.
    # Results may be produced at any time during this phase.
    EXECUTING = 'EXECUTING', _('EXECUTING')

    # the execution of the job is over. The Results may be collected.
    COMPLETED = 'COMPLETED', _('COMPLETED')

    # the job failed to complete. No further work will be done nor Results produced.
    # Results may be unavailable or available but invalid;
    # either way the Results should not be trusted.
    ERROR = 'ERROR', _('ERROR')
    # the job has been manually aborted by the user,
    # or the system has aborted the job due to lack of or overuse of resources.
    ABORTED = 'ABORTED', _('ABORTED')
    # The job is in an unknown state.
    UNKNOWN = 'UNKNOWN', _('UNKNOWN')

    # The job is HELD pending execution and will not automatically be executed (cf, PENDING)
    HELD = 'HELD', _('HELD')

    # The job has been suspended by the system during execution.
    # This might be because of temporary lack of resource.
    # The UWS will automatically resume the job into the EXECUTING phase
    # without any intervention when resource becomes available.
    SUSPENDED = 'SUSPENDED', _('SUSPENDED')

    ARCHIVED = 'ARCHIVED', _('ARCHIVED')


class Owner(models.Model):
    pass


class Job(models.Model):
    jobType = models.CharField(max_length=100)
    jobId = models.IntegerField(auto_created=True)
    runId = models.CharField(max_length=100)
    ownerId = models.ForeignKey(Owner, on_delete=models.SET_NULL, null=True)
    phase = models.CharField(max_length=100, choices=ExecutionPhase.choices)
    quote = models.DateTimeField(null=True)
    startTime = models.DateTimeField(null=True)
    endTime = models.DateTimeField(null=True)
    executionDuration = models.IntegerField(default=0)
    destruction = models.DateTimeField(null=True)

    def execute(self, task_adapter: TaskAdapter):
        parameters = list(self.parameters)
        task_adapter.spawn_job(self.jobType, parameters)

    def execution_status(self, task_adapter: TaskAdapter) -> ExecutionPhase:
        return task_adapter.get_job_phase(self.jobType, self.jobId)

    def process_error(self, task_adapter: TaskAdapter):
        errors = task_adapter.get_job_errors(self.jobType, self.jobId)
        job_errors = [JobError(job=self, message=error) for error in errors]
        JobError.objects.bulk_create(job_errors)

    def archive(self, task_adapter: TaskAdapter):
        task_adapter.delete_job(self.jobType, self.jobId)

        self.phase = ExecutionPhase.ARCHIVED
        self.save()


class Parameter(models.Model):
    job = models.ForeignKey(Job, related_name='parameters', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=10000)


class Result(models.Model):
    job = models.ForeignKey(Job, related_name='results', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100000)


class JobError(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    message = models.TextField()


class JobInfo(models.Model):
    job = models.ForeignKey(Job, related_name='jobInfo', on_delete=models.CASCADE)

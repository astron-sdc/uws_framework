import abc


class TaskAdapter(abc.ABC):
    @abc.abstractmethod
    def spawn_job(self, job_type, parameters):
        pass

    @abc.abstractmethod
    def get_job_phase(self, job_type, job_id):
        pass

    @abc.abstractmethod
    def get_job_errors(self, job_type, job_id):
        pass

    @abc.abstractmethod
    def delete_job(self, job_type, job_id):
        pass

    @abc.abstractmethod
    def get_tasks_list(self):
        pass

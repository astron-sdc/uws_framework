from django.urls import path, include
from .views import *
urlpatterns = [
    path('<str:type>/', JobList.as_view()),
    path('<str:type>/<str:pk>', JobDetails.as_view()),

]
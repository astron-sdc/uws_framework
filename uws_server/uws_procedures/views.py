from django.shortcuts import render
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.request import Request

from .models import Job
from .serializers import JobSerializer


class JobList(CreateAPIView, ListAPIView):
    serializer_class = JobSerializer

    def get_queryset(self):
        return Job.objects.filter(jobType=self.kwargs['type'])


class JobDetails(RetrieveAPIView):
    serializer_class = JobSerializer
    
    def get_queryset(self):
        return Job.objects.filter(jobType=self.kwargs['type'], pk=self.kwargs['pk'])
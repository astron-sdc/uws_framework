#!/bin/env python
import os
import time
from typing import Union
import logging
from argparse import ArgumentParser

# TRICKY DJANGO IMPORT
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'uws_server.settings')
import django
django.setup()
from django.conf import settings
# -------------------

from uws_procedures.models import Job, ExecutionPhase
from uws_procedures.tasks import TaskAdapter

logging.basicConfig(format='%(asctime)-15s - %(levelname)s : %(message)s', level=logging.INFO)

TASK_ADAPTER: Union[TaskAdapter, None] = None
MAX_RUNNING_TASKS: int = 0


def _list_jobs(phase: ExecutionPhase):
    return Job.objects.filter(phase=phase)


def list_running_jobs():
    return _list_jobs(ExecutionPhase.EXECUTING)


def list_pending_jobs():
    return _list_jobs(ExecutionPhase.PENDING)


def list_queued_jobs():
    return _list_jobs(ExecutionPhase.QUEUED)


def setup_execution(args):
    global TASK_ADAPTER, MAX_RUNNING_TASKS
    TASK_ADAPTER = settings.TASK_ADAPTER()
    MAX_RUNNING_TASKS = args.max_running_jobs


def parse_arguments():
    parser = ArgumentParser(description='Schedule background tasks and monitors them')
    parser.add_argument('--sampling_time', type=int, help='sampling time in seconds', default=1)
    parser.add_argument('--max_running_jobs', type=int, help='running execution queue size', default=10)
    return parser.parse_args()


def queue_pending_tasks():
    for job_object in list_pending_jobs():
        logging.info('found pending job, %s, try inserting in queue', job_object.pk)
        if list_queued_jobs().count() < MAX_RUNNING_TASKS:
            job_object.phase = ExecutionPhase.QUEUED
            job_object.save()
        else:
            logging.info('queue full skipping insertion')


def execute_queued_tasks():
    for job_object in list_queued_jobs():
        if list_running_jobs().count() < MAX_RUNNING_TASKS:
            job_object.execute(TASK_ADAPTER)
            job_object.phase = ExecutionPhase.EXECUTING
            job_object.save()


def monitor_running_tasks():
    for job_object in list_running_jobs():
        phase = job_object.execution_status(TASK_ADAPTER)
        job_object.phase = phase
        job_object.save()

        if job_object.phase in [ExecutionPhase.ABORTED, ExecutionPhase.ERROR, ExecutionPhase.COMPLETED]:
            handle_finished_tasks(job_object)


def handle_finished_tasks(job_object: Job):
    if job_object.phase == ExecutionPhase.ERROR:
        job_object.process_error(TASK_ADAPTER)

    job_object.archive(TASK_ADAPTER)


def main_loop():
    queue_pending_tasks()
    execute_queued_tasks()
    monitor_running_tasks()


def main():
    args = parse_arguments()
    setup_execution(args)
    try:
        while True:
            logging.info('starting iteration')
            main_loop()
            logging.info('finished iteration')
            time.sleep(args.sampling_time)
    except KeyboardInterrupt as e:
        raise SystemExit(0)
    except Exception as e:
        logging.exception('Exception: %s occurred - Resuming iteration', e)


if __name__ == '__main__':
    main()

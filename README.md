# Universal Worker Service Framework

Implemented in Django and djangorestframework the uws_framework is meant to enable UWS compatibility of your task
server. The UWS is a IVOA specification for async services _(please find more details
at https://ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html)_


## Quick start
The following assumes that you are in the uws_server directory and that such directory is in the python path of your 
active python environment.
Before deploying the service you have to create the database. Such operation is completely automatic thanks to Django
all you have to do is specify the database server you want to use in the database section of 
`uws_server/settings.py`

After that the migrations have to be created and applied. This can be done with 
```
$ python manage.py makemigrations
$ python manage.py migrate
```

To spawn a test server you can run the command:
```
$ python manage.py runserver
```

Executing the above operations will only enable the rest interface of the UWS framework. To completely use the service
you have to execute also the job scheduler. To do so you have to execute the script

```
$ bin/job_scheduler.py
```

As this is a framework nothing is implemented to the actual execution of the any job. To implement the execution method
you have to implement a task adapter and export it in the `settings.py` file.
An abstract class is available at the path `uws_procedures/tasks.py`. Furthermore, a test implementation is in `test_tasks/tasks.py`.


